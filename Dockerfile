FROM ubuntu:23.04

RUN apt-get update && \
    apt-get install -y clang-format nano openssh-client git python3 python3-pip yapf3
   

### Set the user to be clang
RUN useradd -c 'clang-format-bot' -m -d /home/clang -s /bin/bash clang
USER clang
ENV HOME /home/clang

#RUN ln -s /usr/bin/clang-format-9 /usr/bin/clang-format

COPY scripts /scripts/
COPY style/clang-format.style /.clang-format
COPY style/yapf-format.style  /.style.yapf



CMD bash
